
/**
 * Util class for determining whether
 */
class Is {

    /**
     * Get whether the suppied value is a valid longitude
     * @param value the value to test
     */
    public static longitude(value: any): value is number
    {
        return Is.number(value) && isFinite(value) && Math.abs(value) <= 180;
    }

    /**
     * Get whether the suppied value is a valid latitude
     * @param value the value to test
     */
    public static latitude(value: any): value is number
    {
        return Is.number(value) && isFinite(value) && Math.abs(value) <= 90;
    }

    /**
     * Gets if the specified value is a number.
     * @param value The value to test
     */
    public static number(value: any): value is number
    {
        return value != null && !isNaN(value) && (typeof value === "number" || value instanceof Number);
    }

    /**
     * Gets if the specified value is an integer.
     * Does not type-guard, as integer type currently impossible.
     * @param value The value to test
     */
    public static integer(value: any): boolean
    {
        return Is.number(value) && (value % 1) === 0;
    }
}

export default Is;