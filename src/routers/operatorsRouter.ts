import Router from "koa-router";
import OperatorsController from '../controllers/operatorsController';

/**
 * Initialise the Koa router, route the operators controller return to the /operators slug
 */

const router = new Router();
const operatorsController = new OperatorsController();

router.get('/operators/distance', operatorsController.getOperatorDistance);
router.get('/operators', operatorsController.getOperators);
//router.get('/operators/:id', operatorsController.getOperatorById);
router.put('/operators/:id', operatorsController.updateOperator);

export default router;