import Koa from "koa";
import json from "koa-json";
import logger from "koa-logger";
import operatorsRouter from './routers/operatorsRouter';
import bodyParser from "koa-bodyparser";

/**
 * Starts and configures the server and connects to the database.
 */
class Server 
{
    private _app;

    constructor() 
    {
        this._app = new Koa();

        this._app.use(json());
        this._app.use(logger());
        this._app.use(bodyParser());

        this.routerConfig();
    }
    
    /**
     * Init the router
     */
    private routerConfig() 
    {
        this._app.use(operatorsRouter.routes()).use(operatorsRouter.allowedMethods);
    }

    /**
     * Start the server listening on a specfied port
     * @param port The port to listen to
     */
    public start = (port: number) => 
    {
        return new Promise((resolve, reject) => 
        {
            this._app.listen(port, () => 
            {
                resolve(port);
            }).on('error', (err: Object) => reject(err));
        });
    };
}

export default Server;