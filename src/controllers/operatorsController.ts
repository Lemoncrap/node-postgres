import pool from '../database/pool';
import Is from '../util/Is';

/**
 * The operator controller in charge of the varies queries performed on the database.
 */
class OperatorsController
{

    /**
     * Get all operators in the database.
     * @param ctx 
     * @param next 
     */
    public async getOperators(ctx, next)
    {
        try
        {
            console.log("Get Operators");
            const client = await pool.connect();

            const sql = `SELECT * FROM operators ORDER BY id ASC`;
            const { rows } = await client.query(sql);
            const operators = rows;

            client.release();

            ctx.body = operators;
            console.log(operators);
            await next();
            
        } catch (error)
        {
            console.log(error);
            ctx.status = 400;
            ctx.body = error;
        }
    }

    // /**
    //  * Get the operator with given id.
    //  * @param ctx 
    //  * @param next 
    //  */
    // public async getOperatorById(ctx, next) {
    //     try {
    //         console.log("Get Operator by ID");
    //         const client = await pool.connect();

    //         const sql = `SELECT * FROM operators WHERE id = ${ctx.params.id}`;
    //         const { row } = await client.query(sql);
    //         const operator = row;

    //         client.release();

    //         ctx.body = operator;
    //         console.log(operator);
    //         await next();
    //     } catch (error)
    //     {
    //         console.log(error);
    //         ctx.status = 400;
    //         ctx.body = error;
    //     }
    // }

    /**
     * Creates an operator given a certain id and lat/long value
     * @param ctx 
     * @param next 
     */
    public async updateOperator(ctx, next)
    {
        try
        {
            console.log("Update Operator");

            const id = ctx.params.id;

            if (!Is.integer(+id)) { throw new Error("Incorrect ID specified") }

            const { lat, long } = ctx.request.body;

            if (lat == null || !Is.latitude(lat))
            {
                throw new Error("Latitude is either not supplied or not valid");
            }

            if (long == null || !Is.longitude(long))
            {
                throw new Error("Longitude is either not supplied or not valid");
            }

            const client = await pool.connect();

            const sql = `UPDATE operators SET lat = ${lat}, long = ${long} WHERE id = ${ctx.params.id} RETURNING *`;
            const { rows } = await client.query(sql);
            const operators = rows;

            client.release();

            ctx.body = operators;
            console.log(operators);
            await next();

        } catch (error)
        {
            console.log(error);
            ctx.status = 400;
            ctx.body = error;
        }
    }

    /**
     * Get all operators in the database.
     * @param ctx 
     * @param next 
     */
    public async getOperatorDistance(ctx, next)
    {
        try
        {
            console.log("Get Operator Distance");

            const { lat, long } = ctx.request.query;

            console.log(`lat = ${lat}`);
            console.log(`long = ${long}`);

            if (lat == null || !Is.latitude(+lat))
            {
                throw new Error("Latitude is either not supplied or not valid");
            }

            if (long == null || !Is.longitude(+long))
            {
                throw new Error("Longitude is either not supplied or not valid");
            }

            const client = await pool.connect();

            const sql = `SELECT *, calculate_distance(${lat}, ${long}, lat, long, 'M') as distance FROM operators ORDER BY distance ASC`;
            const { rows } = await client.query(sql);
            const operators = rows;

            client.release();

            ctx.body = operators;
            console.log(operators);

        } catch (error)
        {
            console.log(error);
            ctx.status = 400;
            ctx.body = error;
        }
    }
}

export default OperatorsController;