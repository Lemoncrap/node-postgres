import { Pool } from 'pg';
import dotenv from 'dotenv';

/**
 * Create the pool of database connections
 */

dotenv.config();

export default new Pool ({
    max: 20,
    connectionString: `postgres://${process.env.USER}:${process.env.PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB}`,
    idleTimeoutMillis: 30000
});