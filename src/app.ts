import server from './server';
import dotenv from 'dotenv';

/**
 * Entry point for app, creates the server.
 */

 // Init the .env file
dotenv.config();

const port = parseInt(process.env.PORT || '3000');

const starter = new server().start(port)
    .then(port => console.log(`Running on port ${port}`))
    .catch(error =>
    {
        console.log(error);
    });

export default starter;