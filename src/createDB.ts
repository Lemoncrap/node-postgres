import pool from './database/pool';
import "isomorphic-fetch"

/**
 * Create the database and populate it
 */

const createDB = async () =>
{
    const client = await pool.connect();

    // Create db
    await client.query(
        `CREATE TABLE IF NOT EXISTS operators (
        id SERIAL NOT NULL PRIMARY KEY,
        firstname VARCHAR(30) NOT NULL,
        lastname VARCHAR(30) NOT NULL,
        active BOOLEAN NOT NULL DEFAULT FALSE,
        lat DOUBLE PRECISION,
        long DOUBLE PRECISION)`
    );

    // Fill in with random data, all of this isn't really optimal but fine for the 1 time use.
    // Using the https://randomuser.me/documentation#format api found here which includes name and coordinate data.

    const people = (await (await fetch("https://randomuser.me/api/?inc=name,location&results=100&nat=gb")).json()).results;
    
    for (const person of people)
    {
        const firstname = person.name.first;
        const lastname = person.name.last;
        const lat = person.location.coordinates.latitude;
        const long = person.location.coordinates.longitude;

        await client.query(
            `INSERT INTO operators(firstname, lastname, lat, long)VALUES('${firstname}', '${lastname}', ${lat}, ${long})`
        )
    }    
    
    // Create distance function
    await client.query(
        `CREATE OR REPLACE FUNCTION calculate_distance(lat1 float, lon1 float, lat2 float, lon2 float, units varchar)
        RETURNS float AS $dist$
            DECLARE
                dist float = 0;
                radlat1 float;
                radlat2 float;
                theta float;
                radtheta float;
            BEGIN
                IF lat1 = lat2 OR lon1 = lon2
                    THEN RETURN dist;
                ELSE
                    radlat1 = pi() * lat1 / 180;
                    radlat2 = pi() * lat2 / 180;
                    theta = lon1 - lon2;
                    radtheta = pi() * theta / 180;
                    dist = sin(radlat1) * sin(radlat2) + cos(radlat1) * cos(radlat2) * cos(radtheta);

                    IF dist > 1 THEN dist = 1; END IF;

                    dist = acos(dist);
                    dist = dist * 180 / pi();
                    dist = dist * 60 * 1.1515;

                    IF units = 'K' THEN dist = dist * 1.609344; END IF;
                    IF units = 'N' THEN dist = dist * 0.8684; END IF;

                    RETURN dist;
                END IF;
            END;
        $dist$ LANGUAGE plpgsql;`
    );

    await client.release();
};

createDB();