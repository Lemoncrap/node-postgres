# Node js server using psql

A node server that communicates with a postgres database populated with geo information.

## Usage

In the project root folder, run 

```
npm install
```

Then change the database settings in the .env file, then to create the table run

```
npm run create-database
```

And then the following command to start the node server running

```
npm start
```

The various API endpoints can be be accessed like so

```
GET /operators - Full list of operators in the database
```

```
GET /operators/distance?lat=&long= - Full list of operators in the database, sorted by distance from the specified long/lat
```

```
PUT /operators/:id - Update the lat and long of the specified operator id. Body would be something like { lat: -30.00, long: 130.00  }
```